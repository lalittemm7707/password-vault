import AsyncStorage from "@react-native-async-storage/async-storage";
import axios from "axios";

export const getData = () => {
	return new Promise(async (resolve, reject) => {
		const userData = await AsyncStorage.getItem("user");
		const userId = JSON.parse(userData).sub;
		axios
			.post(`https://j390k4xf9i.execute-api.us-east-1.amazonaws.com/Prod`, {
				userId: userId
			})
			.then(res => {
				if (res.data.statusCode !== 404) {
					resolve(res.data);
				} else {
					resolve(null);
				}
			})
			.catch(err => {
				reject(false);
			});
	});
};

export const uploadData = async data => {
	return new Promise(async (resolve, reject) => {
		const userData = await AsyncStorage.getItem("user");
		const userId = JSON.parse(userData).sub;
		axios
			.post(`https://thsy61t256.execute-api.us-east-1.amazonaws.com/Prod`, {
				data,
				userId
			})
			.then(res => {
				resolve(true);
			});
	});
};
