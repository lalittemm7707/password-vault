import React from "react";
import {
	View,
	Text,
	TouchableOpacity,
	Dimensions,
	StyleSheet,
	StatusBar,
	Image
} from "react-native";
import * as Animatable from "react-native-animatable";
import { LinearGradient } from "expo-linear-gradient";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import { useTheme } from "@react-navigation/native";

const SplashScreen = ({ navigation }) => {
	const { colors } = useTheme();

	return (
		<View style={styles.container}>
			<StatusBar backgroundColor="#009387" barStyle="light-content" />
			<View style={styles.header}>
				<Animatable.Image
					animation="bounceIn"
					duration={1500}
					source={require("../assets/icon.png")}
					style={styles.logo}
				/>
			</View>
			<Animatable.View
				animation="fadeInUpBig"
				duration={1000}
				style={styles.footer}>
				<Text style={[styles.title]}>Never lose your Password!</Text>
				<Text style={styles.text}>
					This app has end to end encryption, We can't read your data.
				</Text>
				<View style={styles.button}>
					<TouchableOpacity onPress={() => navigation.navigate("SignInScreen")}>
						<LinearGradient
							colors={["#08d4c4", "#01ab9d"]}
							style={styles.signIn}>
							<Text style={styles.textSign}>Get Started</Text>
							<MaterialIcons name="navigate-next" color="#fff" size={20} />
						</LinearGradient>
					</TouchableOpacity>
				</View>
			</Animatable.View>
		</View>
	);
};

export default SplashScreen;

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#fff",
		width: "100%"
	},
	header: {
		flex: 2,
		justifyContent: "center",
		alignItems: "center"
	},
	footer: {
		flex: 1,
		backgroundColor: "#009387",
		borderTopLeftRadius: 30,
		borderTopRightRadius: 30,
		paddingVertical: 50,
		paddingHorizontal: 30
	},
	logo: {
		width: 100,
		height: 100,
		borderRadius: 40
	},
	title: {
		color: "#fff",
		fontSize: 30,
		fontWeight: "bold"
	},
	text: {
		color: "white",
		marginTop: 5
	},
	button: {
		alignItems: "flex-end",
		marginTop: 30
	},
	signIn: {
		width: 150,
		height: 40,
		justifyContent: "center",
		alignItems: "center",
		borderRadius: 50,
		flexDirection: "row"
	},
	textSign: {
		color: "white",
		fontWeight: "bold"
	}
});
