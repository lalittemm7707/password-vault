import React, { useState } from "react";
import {
	View,
	Text,
	TouchableOpacity,
	TextInput,
	Platform,
	StyleSheet,
	StatusBar,
	Alert,
	KeyboardAvoidingView,
	ActivityIndicator
} from "react-native";
import * as Animatable from "react-native-animatable";
import { LinearGradient } from "expo-linear-gradient";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import Feather from "react-native-vector-icons/Feather";
import { useTheme } from "@react-navigation/native";
import { Auth } from "aws-amplify";
import { validMailId, validPassword } from "../components/util";
const SignUpScreen = ({ navigation }) => {
	const [name, setName] = useState("");
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [confirmPassword, setConfirmPassword] = useState("");
	const [isError, setError] = useState("");
	const [isLoading, setLoading] = useState(false);

	async function signUp() {
		setError("");
		setLoading(true);
		try {
			const { user } = await Auth.signUp({
				username: email.trim(),
				password: password,
				attributes: {
					email: email, // optional
					name: name // optional - E.164 number convention
					// other custom attributes
				}
			});
			setLoading(false);
			navigation.navigate("SignInScreen");
		} catch (error) {
			setLoading(false);
			setError(error.message);
		}
	}

	function checkForEnableButton() {
		if (
			name === "" ||
			!validMailId(email) ||
			!validPassword(password) ||
			password !== confirmPassword
		)
			return true;
		return false;
	}

	return (
		<View style={styles.container}>
			<View style={styles.header}>
				<Text style={styles.text_header}>Welcome! Please Sign Up</Text>
			</View>
			<Animatable.View animation="fadeInUpBig" style={styles.footer}>
				<Text style={styles.text_footer}>Email</Text>
				<View style={styles.action}>
					<FontAwesome name="user-o" color="#05375a" size={20} />
					<TextInput
						placeholder="Your Email"
						style={styles.textInput}
						autoCapitalize="none"
						value={email}
						onChangeText={e => setEmail(e)}
					/>
					<Animatable.View
						animation={`${validMailId(email) ? "fadeInRight" : "fadeOutRight"}`}
						duration={500}>
						<Feather name="check-circle" color="green" size={20} />
					</Animatable.View>
				</View>
				<Text style={[styles.text_footer, { marginTop: 20 }]}>Name</Text>
				<View style={styles.action}>
					<FontAwesome name="user-o" color="#05375a" size={20} />
					<TextInput
						placeholder="Your Name"
						style={styles.textInput}
						autoCapitalize="words"
						value={name}
						onChangeText={e => setName(e)}
					/>
					<Animatable.View
						animation={`${name !== "" ? "fadeInRight" : "fadeOutRight"}`}
						duration={500}>
						<Feather name="check-circle" color="green" size={20} />
					</Animatable.View>
				</View>
				<Text style={[styles.text_footer, { marginTop: 20 }]}>Password</Text>
				<View style={styles.action}>
					<Feather name="lock" color="#05375a" size={20} />
					<TextInput
						placeholder="********"
						style={styles.textInput}
						autoCapitalize="none"
						secureTextEntry={true}
						value={password}
						onChangeText={e => setPassword(e)}
					/>
					<Animatable.View
						animation={`${
							validPassword(password) ? "fadeInRight" : "fadeOutRight"
						}`}
						duration={500}>
						<Feather name="check-circle" color="green" size={20} />
					</Animatable.View>
				</View>
				<Text style={[styles.text_footer, { marginTop: 20 }]}>
					Confirm Password
				</Text>

				<View style={styles.action}>
					<Feather name="lock" color="#05375a" size={20} />
					<TextInput
						placeholder="confirm password"
						style={styles.textInput}
						autoCapitalize="none"
						value={confirmPassword}
						onChangeText={e => setConfirmPassword(e)}
					/>
					<Animatable.View
						animation={`${
							confirmPassword === password && validPassword(password)
								? "fadeInRight"
								: "fadeOutRight"
						}`}
						duration={500}>
						<Feather name="check-circle" color="green" size={20} />
					</Animatable.View>
				</View>
				{isError !== "" && <Text style={styles.error}>{isError}</Text>}

				<View style={styles.button}>
					<LinearGradient
						colors={
							!checkForEnableButton()
								? ["#08d4c4", "#01ab9d"]
								: ["#E4E4E5", "#D0D0D0"]
						}
						style={styles.signIn}>
						<TouchableOpacity
							onPress={signUp}
							disabled={checkForEnableButton()}>
							{isLoading ? (
								<ActivityIndicator size="small" color="#fff" />
							) : (
								<Text style={[styles.textSign, { color: "#fff" }]}>
									Sign Up
								</Text>
							)}
						</TouchableOpacity>
					</LinearGradient>

					<TouchableOpacity
						style={[
							styles.signIn,
							{
								borderColor: "#009387",
								borderWidth: 1,
								marginTop: 15
							}
						]}
						onPress={() => navigation.navigate("SignInScreen")}>
						<Text
							style={[
								styles.textSign,
								{
									color: "#009387"
								}
							]}>
							Sign In
						</Text>
					</TouchableOpacity>
				</View>
			</Animatable.View>
		</View>
	);
};

export default SignUpScreen;

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#009387"
	},
	header: {
		flex: 1,
		paddingHorizontal: 20,
		paddingBottom: 50,
		justifyContent: "flex-end"
	},
	footer: {
		flex: 3,
		backgroundColor: "#fff",
		borderTopLeftRadius: 30,
		borderTopRightRadius: 30,
		paddingHorizontal: 20,
		paddingVertical: 30
	},
	text_header: {
		color: "#fff",
		fontWeight: "bold",
		fontSize: 30
	},
	text_footer: {
		color: "#05375a",
		fontSize: 18
	},
	action: {
		flexDirection: "row",
		marginTop: 10,
		borderBottomWidth: 1,
		borderBottomColor: "#f2f2f2",
		paddingBottom: 5
	},
	actionError: {
		flexDirection: "row",
		marginTop: 10,
		borderBottomWidth: 1,
		borderBottomColor: "#FF0000",
		paddingBottom: 5
	},
	textInput: {
		flex: 1,
		marginTop: Platform.OS === "ios" ? 0 : -12,
		paddingLeft: 10,
		color: "#05375a"
	},
	errorMsg: {
		color: "#FF0000",
		fontSize: 14
	},
	button: {
		alignItems: "center",
		marginTop: 20
	},
	signIn: {
		width: "100%",
		height: 50,
		justifyContent: "center",
		alignItems: "center",
		borderRadius: 10
	},
	textSign: {
		fontSize: 18,
		fontWeight: "bold"
	},
	error: {
		marginTop: 20,
		color: "#FF5D5D",
		justifyContent: "center",
		alignItems: "center",
		textAlign: "center"
	}
});
