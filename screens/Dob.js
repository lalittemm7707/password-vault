import React, { useState, useRef, useEffect } from "react";
import {
	View,
	Text,
	TouchableOpacity,
	Platform,
	StyleSheet
} from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import * as Animatable from "react-native-animatable";
import OTPTextInput from "react-native-otp-textinput";

const Dob = ({ navigation }) => {
	const [otpValues, setOtpValues] = useState("");
	let otpInput = useRef(null);

	const clearText = () => {
		otpInput.current.clear();
	};

	const setText = e => {
		if (otpInput.current) {
			otpInput.current.setValue(e);
			setOtpValues(e);
		}
	};

	// useEffect(async () => {
	// 	const key = await AsyncStorage.getItem("key");
	// 	if (key) navigation.navigate("ListPassword", { callApi: true });
	// }, []);

	const storeData = async () => {
		try {
			await AsyncStorage.setItem("key", otpValues);
			navigation.navigate("ListPassword", { callApi: true });
		} catch (e) {
			// saving error
		}
	};

	return (
		<View style={styles.container}>
			<View style={styles.header}>
				<Text style={styles.text_header}>Please enter your Date of birth</Text>
			</View>
			<Animatable.View animation="fadeInUpBig" style={styles.footer}>
				<OTPTextInput
					containerStyle={styles.textInputContainer}
					textInputStyle={styles.textInput}
					ref={e => (otpInput = e)}
					inputCount={6}
					handleTextChange={e => {
						setOtpValues(e);
					}}
				/>
				<Text style={styles.info}>We will use this data as Encryption key</Text>
				<TouchableOpacity
					style={[
						styles.signIn,
						{
							borderColor: `${otpValues.length === 6 ? "#009387" : "grey"}`,
							borderWidth: 1,
							marginTop: 15
						}
					]}
					onPress={storeData}
					disabled={otpValues.length !== 6}>
					<Text
						style={[
							styles.textSign,
							{
								color: `${otpValues.length === 6 ? "#009387" : "grey"}`
							}
						]}>
						Proceed
					</Text>
				</TouchableOpacity>
			</Animatable.View>
		</View>
	);
};

export default Dob;

const styles = StyleSheet.create({
	container: {
		flex: 2,
		backgroundColor: "#009387"
	},
	header: {
		flex: 2,
		justifyContent: "flex-end",
		paddingHorizontal: 20,
		paddingBottom: 50
	},
	footer: {
		flex: 3,
		backgroundColor: "#fff",
		borderTopLeftRadius: 30,
		borderTopRightRadius: 30,
		paddingHorizontal: 20,
		paddingVertical: 50
	},
	text_header: {
		color: "#fff",
		fontWeight: "bold",
		fontSize: 30
	},
	text_footer: {
		color: "#05375a",
		fontSize: 18
	},
	action: {
		flexDirection: "row",
		marginTop: 10,
		borderBottomWidth: 1,
		borderBottomColor: "#f2f2f2",
		paddingBottom: 5
	},
	actionError: {
		flexDirection: "row",
		marginTop: 10,
		borderBottomWidth: 1,
		borderBottomColor: "#FF0000",
		paddingBottom: 5
	},
	textInput: {
		flex: 1,
		marginTop: Platform.OS === "ios" ? 0 : -12,
		paddingLeft: 10,
		color: "#05375a"
	},
	errorMsg: {
		color: "#FF0000",
		fontSize: 14
	},
	button: {
		alignItems: "center",
		marginTop: 50
	},
	signIn: {
		width: "100%",
		height: 50,
		justifyContent: "center",
		alignItems: "center",
		borderRadius: 10
	},
	textSign: {
		fontSize: 18,
		fontWeight: "bold"
	},
	textInputContainer: {
		marginBottom: 20,
		color: "#fff"
	},
	textInput: {
		color: "#000000",
		fontSize: 20,
		textAlign: "center"
	},
	info: {
		marginVertical: 30,
		alignItems: "flex-end",
		color: "#009387"
	}
});
