import React, { useState, useEffect, Fragment } from "react";
import {
	View,
	Text,
	TouchableOpacity,
	Dimensions,
	StyleSheet,
	Image,
	ActivityIndicator,
	ScrollView,
	BackHandler,
	Alert
} from "react-native";
import * as Animatable from "react-native-animatable";
import { useTheme } from "@react-navigation/native";
import Feather from "react-native-vector-icons/Feather";
import Add from "../components/Add";
import ViewPassword from "../components/View";
import { getAllData } from "../components/util";
import { useIsFocused } from "@react-navigation/native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import PasswordInfo from "../components/PassworInfo";

const ListPassword = ({ route, navigation }) => {
	const [originalData, setOriginalData] = useState([]);
	const [data, setData] = useState([]);
	const [isLoading, setLoading] = useState(false);
	const [selectedData, setSelectedData] = useState(null);
	const { colors } = useTheme();
	const [showEdit, setShowEdit] = useState(false);
	const [reRender, setReRender] = useState(false);

	const isFocused = useIsFocused();

	useEffect(() => {
		return () => {
			setData([]);
			setShowEdit(false);
		};
	}, []);

	useEffect(() => {
		if (route.params && route.params.callApi) {
			setReRender(true);
		}
	}, [route.params && route.params.callApi]);

	useEffect(() => {
		setLoading(true);
		setData([]);
		getAllData()
			.then(res => {
				setOriginalData(res);
				setData(JSON.parse(res));
				setLoading(false);
				setReRender(false);
			})
			.catch(err => handleLogout());
	}, [reRender]);

	const handlePasswordClick = data => {
		setSelectedData(data);
		setShowEdit(true);
	};

	const handleLogout = async () => {
		await AsyncStorage.clear();
		// NativeModules.DevSettings.reload();
		navigation.navigate("Home");
	};

	useEffect(() => {
		const backAction = () => {
			Alert.alert("Hold on!", "Are you sure you want to go back?", [
				{
					text: "Cancel",
					onPress: () => null,
					style: "cancel"
				},
				{ text: "YES", onPress: () => BackHandler.exitApp() }
			]);
			return true;
		};

		const backHandler = BackHandler.addEventListener(
			"hardwareBackPress",
			backAction
		);

		return () => backHandler.remove();
	}, []);

	try {
		return (
			<View style={styles.container}>
				<View style={[styles.mainHeader, { flex: showEdit ? 3 : 1 }]}>
					<View style={styles.header}>
						<Text style={styles.title}>Password</Text>
						<TouchableOpacity onPress={handleLogout} style={styles.logOut}>
							<Feather name="power" color="#000" size={20} />
						</TouchableOpacity>
					</View>

					<ScrollView>
						{!isLoading ? (
							<Fragment>
								<PasswordInfo
									data={data}
									setData={setData}
									originalData={originalData}
								/>
								<View style={styles.listContainer}>
									{data.map(list => (
										<SingleList
											list={list}
											key={list.id}
											handlePasswordClick={handlePasswordClick}
										/>
									))}
								</View>
							</Fragment>
						) : (
							<ActivityIndicator
								size="large"
								color="green"
								style={styles.loader}
							/>
						)}
					</ScrollView>
				</View>
				{showEdit ? (
					<Animatable.View
						animation="fadeInUpBig"
						duration={500}
						style={[styles.footer, { flex: showEdit ? 3 : 0 }]}>
						<TouchableOpacity
							onPress={() => setShowEdit(false)}
							style={styles.close}>
							<Feather name="x" color="#fff" size={20} />
						</TouchableOpacity>
						<ViewPassword
							navigation={navigation}
							selectedData={selectedData}
							setShowEdit={setShowEdit}
							setReRender={setReRender}
						/>
					</Animatable.View>
				) : (
					<TouchableOpacity
						onPress={() => navigation.navigate("Add", { selectedItem: null })}
						style={styles.floatButton}>
						<Feather name="plus" color="#fff" size={20} />
					</TouchableOpacity>
				)}
			</View>
		);
	} catch {
		handleLogout();
		return null;
	}
};

const SingleList = ({ list, handlePasswordClick }) => {
	var diffDays = parseInt(
		(new Date() - new Date(list.edited || list.date)) / (1000 * 60 * 60 * 24),
		10
	);
	const img =
		list.imageUrl !== ""
			? list.imageUrl
			: "https://img.icons8.com/plasticine/2x/no-image.png";
	return (
		<View style={styles.list}>
			<Image
				source={{
					uri: img
				}}
				style={styles.logo}
				resizeMode="stretch"
			/>
			<TouchableOpacity
				onPress={() => {
					handlePasswordClick(list);
				}}>
				<View style={styles.listInfo}>
					<Text style={styles.listMainText}>{list.domain}</Text>
					<Text style={styles.listSecText}>
						{list.edited
							? diffDays === 0
								? `Edited Today`
								: `Edited ${diffDays} days ago`
							: diffDays === 0
							? `Added Today`
							: `Added ${diffDays} days ago`}
					</Text>
				</View>
			</TouchableOpacity>
		</View>
	);
};

const data = [
	{
		link: "keep.google.com",
		update: "Added 3 days ago",
		img: "https://img.icons8.com/color/452/google-logo.png"
	},
	{
		link: "invisionapp.com",
		update: "Added 2 days ago",
		img: "https://cdn.iconscout.com/icon/free/png-512/invision-studio-2719772-2265518.png"
	},
	{
		link: "shopify.com",
		update: "Added 5 days ago",
		img: "https://image.flaticon.com/icons/png/512/825/825500.png"
	},
	{
		link: "paypal.com",
		update: "Added 2 days ago",
		img: "https://icons-for-free.com/iconfiles/png/512/finance+money+pay+payment+paypal+icon-1320086082799997084.png"
	}
];

export default ListPassword;

const { height } = Dimensions.get("screen");
const height_logo = height * 0.28;

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#fff",
		width: "100%"
	},
	header: {
		marginTop: 40,
		marginLeft: 20
	},
	mainHeader: {},
	footer: {
		backgroundColor: "#009387",
		borderTopLeftRadius: 30,
		borderTopRightRadius: 30,
		paddingVertical: 50,
		paddingHorizontal: 30
	},
	search: {
		flexDirection: "row",
		margin: 30,
		paddingLeft: 10,
		color: "#05375a",
		backgroundColor: "#A0D1F9",
		opacity: 0.4,
		padding: 15,
		paddingStart: 20,
		borderRadius: 20
	},

	listContainer: {
		margin: 30
	},
	list: {
		flexDirection: "row",
		marginBottom: 25
	},
	logo: {
		marginRight: 5,
		height: 50,
		width: 50,
		borderRadius: 50
	},
	listInfo: {
		marginLeft: 10
	},
	listMainText: {
		fontSize: 22,
		fontWeight: "bold"
	},
	listSecText: {
		fontSize: 18,
		marginTop: 5,
		color: "grey"
	},
	floatButton: {
		width: 60,
		height: 60,
		alignItems: "center",
		justifyContent: "center",
		shadowColor: "#333",
		shadowOpacity: 0.1,
		shadowOffset: { x: 2, y: 0 },
		shadowRadius: 2,
		borderRadius: 30,
		position: "absolute",
		bottom: 20,
		right: 20,
		backgroundColor: "#009387"
	},
	close: {
		position: "absolute",
		right: 10,
		top: 20,
		fontSize: 20
	},
	title: {
		color: "#05375a",
		fontSize: 30,
		fontWeight: "bold"
	},
	text: {
		color: "grey",
		marginTop: 5
	},

	textSign: {
		color: "white",
		fontWeight: "bold"
	},
	textInput: {
		paddingStart: 10
	},
	logOut: {
		width: 40,
		height: 40,
		alignItems: "center",
		justifyContent: "center",
		shadowColor: "#333",
		shadowOpacity: 0.1,
		shadowRadius: 2,
		borderRadius: 30,
		position: "absolute",
		bottom: 0,
		right: 20,
		color: "#fff",
		backgroundColor: "#DFE4E4"
	},
	loader: {
		alignItems: "center",
		justifyContent: "center"
	}
});
