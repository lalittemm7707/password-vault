import "react-native-gesture-handler";
import * as React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { StyleSheet, Text, View } from "react-native";
import SplashScreen from "./SplashScreen";
import SignInScreen from "./Login";
import ListPassword from "./ListPassword";
import Add from "../components/Add";
import SignUpScreen from "./SignUp";
import Dob from "./Dob";
import { useEffect } from "react";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { useState } from "react";

const Stack = createStackNavigator();
export default function Main() {
	const [initialRoute, setInitial] = useState(null);
	useEffect(() => {
		getData().then(res => {
			if (res.value && res.userData) {
				setInitial("ListPassword");
			} else if (res.value && !userData) {
				setInitial("dob");
			} else {
				setInitial("Home");
			}
		});
	}, []);
	const getData = async () => {
		try {
			const value = await AsyncStorage.getItem("key");
			const userData = await AsyncStorage.getItem("user");
			return { value, userData };
		} catch (e) {
			return null;
		}
	};
	if (initialRoute)
		return (
			<NavigationContainer>
				<Stack.Navigator
					screenOptions={{
						headerShown: false
					}}
					initialRouteName={initialRoute}>
					<Stack.Screen name="Home" component={SplashScreen} />
					<Stack.Screen name="SignInScreen" component={SignInScreen} />
					<Stack.Screen name="ListPassword" component={ListPassword} />
					<Stack.Screen name="Add" component={Add} />
					<Stack.Screen name="SignUp" component={SignUpScreen} />
					<Stack.Screen name="dob" component={Dob} />
				</Stack.Navigator>
			</NavigationContainer>
		);
	return null;
}

const styles = {
	container: {
		flex: 1,
		backgroundColor: "#009387"
	}
};
