import React, { useEffect, useState } from "react";
import {
	View,
	Text,
	TouchableOpacity,
	TextInput,
	Platform,
	StyleSheet,
	StatusBar,
	Alert,
	ActivityIndicator,
	Vibration
} from "react-native";
import * as Animatable from "react-native-animatable";
import { LinearGradient } from "expo-linear-gradient";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import Feather from "react-native-vector-icons/Feather";
import { Auth } from "aws-amplify";
import { validMailId } from "../components/util";
import AsyncStorage from "@react-native-async-storage/async-storage";
const SignInScreen = ({ navigation }) => {
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [isError, setError] = useState("");
	const [isLoading, setLoading] = useState(false);
	const [showPassword, setShowPassword] = useState(false);

	useEffect(() => {
		return () => {
			setEmail("");
			setPassword("");
			setError("");
			setLoading(false);
			setShowPassword(false);
		};
	}, []);
	async function signIn() {
		setLoading(true);
		setError("");
		try {
			const user = await Auth.signIn(email.trim(), password.trim());
			setLoading(false);
			storeData(JSON.stringify(user.attributes));
		} catch (error) {
			Vibration.vibrate();
			setLoading(false);
			setError(error.message);
		}
	}

	function checkForEnableButton() {
		if (!validMailId(email) || password === "") return true;
		return false;
	}

	const storeData = async userData => {
		try {
			await AsyncStorage.setItem("user", userData);
			navigation.navigate("dob");
		} catch (e) {
			// saving error
		}
	};
	return (
		<View style={styles.container}>
			<View style={styles.header}>
				<Text style={styles.text_header}>Welcome! Please Login</Text>
			</View>
			<Animatable.View animation="fadeInUpBig" style={styles.footer}>
				<Text style={styles.text_footer}>Email</Text>
				<View style={styles.action}>
					<FontAwesome name="user-o" color="#05375a" size={20} />
					<TextInput
						placeholder="Your Email"
						style={styles.textInput}
						autoCapitalize="none"
						value={email}
						onChangeText={e => setEmail(e)}
					/>
					<Animatable.View
						animation={`${validMailId(email) ? "fadeInRight" : "fadeOutRight"}`}
						duration={500}>
						<Feather name="check-circle" color="green" size={20} />
					</Animatable.View>
				</View>
				<Text style={[styles.text_footer, { marginTop: 35 }]}>Password</Text>
				<View style={styles.action}>
					<Feather name="lock" color="#05375a" size={20} />
					<TextInput
						placeholder="********"
						style={styles.textInput}
						autoCapitalize="none"
						secureTextEntry={!showPassword}
						value={password}
						onChangeText={e => setPassword(e)}
					/>
					<TouchableOpacity onPress={() => setShowPassword(!showPassword)}>
						<Feather
							name={!showPassword ? "eye-off" : "eye"}
							color={!showPassword ? "grey" : "#009387"}
							size={20}
						/>
					</TouchableOpacity>
				</View>
				{isError !== "" && <Text style={styles.error}>{isError}</Text>}
				<TouchableOpacity>
					<Text style={{ color: "#009387", marginTop: 15 }}>
						Forgot password?
					</Text>
				</TouchableOpacity>
				<View style={styles.button}>
					<LinearGradient
						colors={
							!checkForEnableButton()
								? ["#08d4c4", "#01ab9d"]
								: ["#E4E4E5", "#D0D0D0"]
						}
						style={styles.signIn}>
						<TouchableOpacity
							onPress={signIn}
							disabled={checkForEnableButton()}>
							{isLoading ? (
								<ActivityIndicator size="small" color="#fff" />
							) : (
								<Text style={[styles.textSign, { color: "#fff" }]}>
									Sign In
								</Text>
							)}
						</TouchableOpacity>
					</LinearGradient>

					<TouchableOpacity
						style={[
							styles.signIn,
							{
								borderColor: "#009387",
								borderWidth: 1,
								marginTop: 15
							}
						]}
						onPress={() => navigation.navigate("SignUp")}>
						<Text
							style={[
								styles.textSign,
								{
									color: "#009387"
								}
							]}>
							Sign Up
						</Text>
					</TouchableOpacity>
				</View>
			</Animatable.View>
		</View>
	);
};

export default SignInScreen;

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#009387"
	},
	header: {
		flex: 1,
		justifyContent: "flex-end",
		paddingHorizontal: 20,
		paddingBottom: 50
	},
	footer: {
		flex: 3,
		backgroundColor: "#fff",
		borderTopLeftRadius: 30,
		borderTopRightRadius: 30,
		paddingHorizontal: 20,
		paddingVertical: 30
	},
	text_header: {
		color: "#fff",
		fontWeight: "bold",
		fontSize: 30
	},
	text_footer: {
		color: "#05375a",
		fontSize: 18
	},
	action: {
		flexDirection: "row",
		marginTop: 10,
		borderBottomWidth: 1,
		borderBottomColor: "#f2f2f2",
		paddingBottom: 5
	},
	actionError: {
		flexDirection: "row",
		marginTop: 10,
		borderBottomWidth: 1,
		borderBottomColor: "#FF0000",
		paddingBottom: 5
	},
	textInput: {
		flex: 1,
		marginTop: Platform.OS === "ios" ? 0 : -12,
		paddingLeft: 10,
		color: "#05375a"
	},
	errorMsg: {
		color: "#FF0000",
		fontSize: 14
	},
	button: {
		alignItems: "center",
		marginTop: 20
	},
	signIn: {
		width: "100%",
		height: 50,
		justifyContent: "center",
		alignItems: "center",
		borderRadius: 10
	},
	textSign: {
		fontSize: 18,
		fontWeight: "bold"
	},
	error: {
		marginTop: 20,
		color: "#FF5D5D",
		justifyContent: "center",
		alignItems: "center",
		textAlign: "center"
	}
});
