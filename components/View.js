import React from "react";
import { useRef, useEffect } from "react";
import {
	View,
	Text,
	TouchableOpacity,
	TextInput,
	Platform,
	StyleSheet,
	StatusBar,
	Alert,
	Image
} from "react-native";
import OTPTextView from "react-native-otp-textinput";
import * as Animatable from "react-native-animatable";
import { LinearGradient } from "expo-linear-gradient";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import { useState } from "react";
import { deletePassword, generateUUID, validateOTP } from "./util";

function ViewPassword({ navigation, selectedData, setShowEdit, setReRender }) {
	console.log({ selectedData });
	const [optSuccess, setOtpSuccess] = useState(false);
	const [otpInput, setOtpInput] = useState("");

	useEffect(() => {
		return () => {
			setOtpSuccess(false);
		};
	}, []);

	const handleDeletePassword = id => {
		deletePassword(selectedData.id).then(res => {
			setShowEdit(false);
			setReRender(generateUUID());
		});
	};

	const verifyOtp = async () => {
		validateOTP(otpInput)
			.then(res => {
				setOtpSuccess(true);
			})
			.catch(err => {
				setOtpSuccess(err);
				createThreeButtonAlert();
			});
	};

	const createThreeButtonAlert = () =>
		Alert.alert(
			"Wrong Pin",
			"You have entered the wrong pin, Please enter last 4 number of your App key",
			[
				{
					text: "Cancel",
					onPress: () => setOtpInput(""),
					style: "cancel"
				}
			]
		);

	const deleteButtonAlert = () =>
		Alert.alert(
			selectedData.domain,
			"Are you sure you wanted to remove this password",
			[
				{
					text: "Cancel",
					onPress: () => console.log("Cancel Pressed"),
					style: "cancel"
				},
				{
					text: "Ok",
					onPress: () => handleDeletePassword(selectedData.id)
				}
			]
		);

	return (
		<View style={styles.container}>
			{!optSuccess ? (
				<>
					<View style={styles.imageContainer}>
						<Image
							source={{
								uri:
									selectedData.imageUrl ||
									"https://img.icons8.com/plasticine/2x/no-image.png"
							}}
							style={styles.logo}
						/>
					</View>
					<View style={styles.otp}>
						<OTPTextView
							containerStyle={styles.textInputContainer}
							textInputStyle={styles.textInput}
							// ref={e => (otpInput = e)}
							handleTextChange={e => setOtpInput(e)}
						/>
						<Text style={styles.info}>
							Please enter last four number of your App key to view
						</Text>

						<View style={styles.button}>
							<TouchableOpacity
								onPress={() => verifyOtp()}
								disabled={otpInput.length !== 4}>
								<LinearGradient
									colors={["#F9FBFB", "#DFE4E4"]}
									style={styles.signIn}>
									<Text style={[styles.textSign]}>Verify</Text>
								</LinearGradient>
							</TouchableOpacity>
						</View>
					</View>
				</>
			) : (
				<Animatable.View
					animation="fadeInRight"
					duration={500}
					style={styles.view}>
					<View style={styles.imageContainer}>
						<Image
							source={{
								uri:
									selectedData.imageUrl ||
									"https://img.icons8.com/plasticine/2x/no-image.png"
							}}
							style={styles.logo}
						/>
					</View>
					<Text style={styles.domain}>{selectedData.domain}</Text>
					<Text style={styles.username}>{selectedData.userName}</Text>
					<Text style={styles.password}>{selectedData.password}</Text>
					<View style={styles.action}>
						<TouchableOpacity
							onPress={() => {
								setShowEdit(false);
								navigation.navigate("Add", { selectedItem: selectedData });
							}}>
							<LinearGradient
								colors={["#F9FBFB", "#DFE4E4"]}
								style={styles.edit}>
								<Text style={[styles.textSign]}>Edit</Text>
							</LinearGradient>
						</TouchableOpacity>
						<TouchableOpacity onPress={deleteButtonAlert}>
							<LinearGradient
								colors={["#F9FBFB", "#DFE4E4"]}
								style={styles.delete}>
								<Text style={[styles.textSign]}>Delete</Text>
							</LinearGradient>
						</TouchableOpacity>
					</View>
				</Animatable.View>
			)}
		</View>
	);
}
const img = {
	1: "https://creazilla-store.fra1.digitaloceanspaces.com/cliparts/79024/lock-icon-clipart-lg.png",
	2: "https://icons-for-free.com/iconfiles/png/512/key-131988534711244269.png"
};

const styles = {
	container: {
		color: "#fff"
	},
	textInputContainer: {
		marginBottom: 20,
		color: "#fff"
	},
	textInput: {
		color: "#fff",
		textAlign: "center"
	},
	button: {
		alignItems: "center",
		marginTop: 50
	},
	signIn: {
		width: 150,
		height: 50,
		justifyContent: "center",
		alignItems: "center",
		borderRadius: 10
	},
	textSign: {
		fontSize: 18,
		fontWeight: "bold",
		color: "#009387"
	},
	imageContainer: {
		justifyContent: "center",
		alignItems: "center"
	},
	logo: {
		width: 100,
		height: 100,
		borderRadius: 50,
		backgroundColor: "#fff",
		borderRadius: 50
	},
	view: {
		marginTop: 40,
		justifyContent: "center",
		alignItems: "center"
	},
	domain: {
		fontSize: 25,
		color: "#fff",
		fontWeight: "bold",
		marginVertical: 20
	},
	username: {
		fontSize: 20,
		color: "#fff",
		fontWeight: "400",
		marginVertical: 10
	},
	password: {
		fontSize: 25,
		color: "#fff",
		fontWeight: "400",
		marginVertical: 10
	},
	action: {
		marginTop: 20,
		flexDirection: "row",
		flexWrap: "wrap"
	},
	edit: {
		width: 150,
		height: 50,
		justifyContent: "center",
		alignItems: "center",
		borderRadius: 10,
		margin: 5
	},
	delete: {
		margin: 5,
		width: 150,
		height: 50,
		justifyContent: "center",
		alignItems: "center",
		borderRadius: 10
	},
	info: {
		marginTop: 20,
		left: 15,
		color: "#fff",
		justifyContent: "center",
		alignItems: "center"
	},
	error: {
		marginTop: 20,
		left: 95,
		color: "#FF5733",
		justifyContent: "center",
		alignItems: "center"
	}
};

export default ViewPassword;
