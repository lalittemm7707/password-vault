import React, { useState, useEffect } from "react";
import {
	View,
	Text,
	TouchableOpacity,
	TextInput,
	Platform,
	StyleSheet,
	StatusBar,
	Alert,
	ActivityIndicator
} from "react-native";
import * as Animatable from "react-native-animatable";
import { LinearGradient } from "expo-linear-gradient";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import Feather from "react-native-vector-icons/Feather";
import { useTheme } from "@react-navigation/native";
import {
	validUrl,
	validPassword,
	generateUUID,
	setNewData,
	editData
} from "./util";
const Add = ({ route, navigation }) => {
	const { selectedItem = null } = route.params;
	const [domain, setDomain] = useState("");
	const [userName, setUserName] = useState("");
	const [imageUrl, setImageUrl] = useState("");
	const [password, setPassword] = useState("");
	const [loader, setLoader] = useState(false);

	useEffect(() => {
		if (selectedItem) {
			setDomain(selectedItem.domain);
			setUserName(selectedItem.userName);
			setImageUrl(selectedItem.imageUrl);
			setPassword(selectedItem.password);
		}
	}, [selectedItem]);
	function checkForEnableButton() {
		if (userName === "" || !validUrl(domain) || password === "") return true;
		return false;
	}

	const AddNewPassword = () => {
		setLoader(true);
		const tempData = {
			domain,
			userName,
			imageUrl,
			password,
			date: new Date(),
			id: generateUUID(),
			edited: null
		};
		setNewData(tempData).then(res => {
			setLoader(false);
			if (res) navigation.navigate("ListPassword", { callApi: generateUUID() });
		});
	};

	const editPassword = () => {
		setLoader(true);
		const tempData = {
			domain,
			userName,
			imageUrl,
			password,
			date: selectedItem.date,
			edited: new Date(),
			id: selectedItem.id
		};
		editData(tempData).then(res => {
			setLoader(false);
			if (res) navigation.navigate("ListPassword", { callApi: generateUUID() });
		});
	};

	return (
		<Animatable.View style={styles.container} animation="fadeInUpBig">
			<View style={styles.header}>
				<Text style={styles.title}>
					{selectedItem ? "Edit " : "Add new "}password
				</Text>
				<TouchableOpacity
					onPress={() => {
						navigation.navigate("ListPassword");
					}}
					style={styles.floatButton}>
					<Feather name="x" color="#000" size={20} />
				</TouchableOpacity>
			</View>
			<Animatable.View>
				<Text style={styles.text_footer}>Domain</Text>
				<View style={styles.action}>
					<FontAwesome name="link" color="#05375a" size={20} />
					<TextInput
						placeholder="Password Domain"
						style={styles.textInput}
						autoCapitalize="none"
						value={domain}
						onChangeText={e => setDomain(e)}
					/>
					<Animatable.View
						animation={`${validUrl(domain) ? "fadeInRight" : "fadeOutRight"}`}
						duration={500}>
						<Feather name="check-circle" color="green" size={20} />
					</Animatable.View>
				</View>
				<Text style={[styles.text_footer, { marginTop: 35 }]}>Username</Text>
				<View style={styles.action}>
					<FontAwesome name="user-o" color="#05375a" size={20} />
					<TextInput
						placeholder="Your username"
						style={styles.textInput}
						autoCapitalize="none"
						value={userName}
						onChangeText={e => setUserName(e)}
					/>
					<Animatable.View
						animation={`${userName !== "" ? "fadeInRight" : "fadeOutRight"}`}
						duration={500}>
						<Feather name="check-circle" color="green" size={20} />
					</Animatable.View>
				</View>

				<Text style={[styles.text_footer, { marginTop: 35 }]}>Image Url</Text>
				<View style={styles.action}>
					<FontAwesome name="image" color="#05375a" size={20} />
					<TextInput
						placeholder="your domain icon"
						style={styles.textInput}
						autoCapitalize="none"
						value={imageUrl}
						onChangeText={e => setImageUrl(e)}
					/>

					<Feather name="check-circle" color="green" size={20} />
				</View>

				<Text style={[styles.text_footer, { marginTop: 35 }]}>Password</Text>
				<View style={styles.action}>
					<Feather name="lock" color="#05375a" size={20} />
					<TextInput
						placeholder="your password"
						style={styles.textInput}
						autoCapitalize="none"
						value={password}
						onChangeText={e => setPassword(e)}
					/>
					<Animatable.View
						animation={`${password !== "" ? "fadeInRight" : "fadeOutRight"}`}
						duration={500}>
						<Feather name="check-circle" color="green" size={20} />
					</Animatable.View>
				</View>
				{password.length ? (
					<Text style={[{ color: validPassword(password) ? "green" : "red" }]}>
						password strength : {validPassword(password) ? "Strong" : "weak"}
					</Text>
				) : null}
				<View style={styles.button}>
					<LinearGradient
						colors={
							!checkForEnableButton()
								? ["#08d4c4", "#01ab9d"]
								: ["#E4E4E5", "#D0D0D0"]
						}
						style={styles.signIn}>
						<TouchableOpacity
							disabled={checkForEnableButton()}
							onPress={() =>
								selectedItem ? editPassword() : AddNewPassword()
							}>
							{loader ? (
								<ActivityIndicator size="large" color="#fff" />
							) : (
								<Text style={[styles.textSign, { color: "#fff" }]}>Save</Text>
							)}
						</TouchableOpacity>
					</LinearGradient>
				</View>
			</Animatable.View>
		</Animatable.View>
	);
};

export default Add;

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#009387",
		backgroundColor: "#fff",
		paddingVertical: 50,
		paddingHorizontal: 30
	},
	title: {
		fontWeight: "bold",
		fontSize: 30
	},
	header: {
		flexDirection: "row",
		paddingBottom: 30
	},
	footer: {
		backgroundColor: "#fff",
		borderTopLeftRadius: 30,
		borderTopRightRadius: 30,
		paddingVertical: 30
	},
	text_header: {
		color: "#fff",
		fontWeight: "bold",
		fontSize: 30
	},
	text_footer: {
		color: "#05375a",
		fontSize: 18
	},
	action: {
		flexDirection: "row",
		marginTop: 10,
		borderBottomWidth: 1,
		borderBottomColor: "#f2f2f2",
		paddingBottom: 5
	},
	actionError: {
		flexDirection: "row",
		marginTop: 10,
		borderBottomWidth: 1,
		borderBottomColor: "#FF0000",
		paddingBottom: 5
	},
	textInput: {
		flex: 1,
		marginTop: Platform.OS === "ios" ? 0 : -12,
		paddingLeft: 10,
		color: "#05375a"
	},
	errorMsg: {
		color: "#FF0000",
		fontSize: 14
	},
	button: {
		alignItems: "center",
		marginTop: 50
	},
	close: {
		position: "absolute",
		right: 10,
		top: 10,
		fontSize: 30,
		borderRadius: 50,
		borderColor: "grey"
	},
	signIn: {
		width: "100%",
		height: 50,
		justifyContent: "center",
		alignItems: "center",
		borderRadius: 10
	},
	textSign: {
		fontSize: 18,
		fontWeight: "bold"
	},
	floatButton: {
		width: 40,
		height: 40,
		alignItems: "center",
		justifyContent: "center",
		shadowColor: "#333",
		shadowOpacity: 0.1,
		shadowRadius: 2,
		borderRadius: 30,
		position: "absolute",
		bottom: 20,
		right: 0,
		color: "#fff",
		backgroundColor: "#DFE4E4"
	}
});
