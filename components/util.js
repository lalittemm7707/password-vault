import AsyncStorage from "@react-native-async-storage/async-storage";
import { getData, uploadData } from "../api/api";
export async function encryption(str) {
	const key = await AsyncStorage.getItem("key");
	var pos = 0;
	let ostr = "";
	while (pos < str.length) {
		ostr = ostr + String.fromCharCode(str.charCodeAt(pos) ^ key);
		pos += 1;
	}
	return ostr;
}

export async function decryption(str) {
	const key = await AsyncStorage.getItem("key");
	var pos = 0;
	let ostr = "";
	while (pos < str.length) {
		ostr = ostr + String.fromCharCode(key ^ str.charCodeAt(pos));
		pos += 1;
	}
	return ostr;
}

export function validMailId(email) {
	var re = /\S+@\S+\.\S+/;
	return re.test(email);
}

export function validPassword(password) {
	const pattern = /^(?=.{5,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[\W])/;
	return pattern.test(password) && password.length >= 8;
}

export function validUrl(url) {
	const pattern =
		/[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi;
	return pattern.test(url);
}

export async function setNewData(data) {
	return new Promise((resolve, reject) => {
		getAllData().then(res => {
			res = JSON.parse(res);
			console.log({ res });
			const newData = [...res, data];
			encryption(JSON.stringify(newData)).then(res => {
				uploadData(res).then(res => {
					resolve(res);
				});
			});
		});
	});
}

export async function deletePassword(id) {
	await getAllData().then(async res => {
		res = JSON.parse(res);
		const newData = res.filter(i => i.id !== id);
		await encryption(JSON.stringify(newData)).then(async res => {
			await uploadData(res).then(res => true);
		});
	});
}

export async function editData(data) {
	return new Promise((resolve, reject) => {
		getAllData().then(async res => {
			res = JSON.parse(res);
			const newData = res;
			res.map((i, e) => {
				if (i.id === data.id) {
					newData[e] = data;
				}
			});
			encryption(JSON.stringify(newData)).then(res => {
				uploadData(res)
					.then(res => {
						resolve(res);
					})
					.catch(err => reject(false));
			});
		});
	});
}

export async function getAllData() {
	return new Promise(resolve => {
		getData()
			.then(key => {
				if (key) {
					decryption(key).then(res => {
						resolve(res);
					});
				} else {
					resolve(JSON.stringify([]));
				}
			})
			.catch(err => reject(err));
	});
}

export function generateUUID() {
	// Public Domain/MIT
	var d = new Date().getTime(); //Timestamp
	var d2 = (performance && performance.now && performance.now() * 1000) || 0; //Time in microseconds since page-load or 0 if unsupported
	return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function (c) {
		var r = Math.random() * 16; //random number between 0 and 16
		if (d > 0) {
			//Use timestamp until depleted
			r = (d + r) % 16 | 0;
			d = Math.floor(d / 16);
		} else {
			//Use microseconds since page-load if supported
			r = (d2 + r) % 16 | 0;
			d2 = Math.floor(d2 / 16);
		}
		return (c === "x" ? r : (r & 0x3) | 0x8).toString(16);
	});
}

export async function validateOTP(otp) {
	const key = await AsyncStorage.getItem("key");
	return new Promise((resolve, reject) => {
		key.slice(2, 6) == otp ? resolve(true) : reject(false);
	});
}
