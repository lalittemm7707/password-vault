import React, { useEffect, useState } from "react";
import { View, Text, TextInput } from "react-native";
import Feather from "react-native-vector-icons/Feather";
import { TouchableOpacity } from "react-native-gesture-handler";
import { validPassword } from "./util";

function PasswordInfo({ data, setData, originalData }) {
	const [compromisedData, setCompromisedData] = useState(0);
	const [isFiltered, setFiltered] = useState(false);

	useEffect(() => {
		return () => {
			setFiltered(false);
		};
	}, []);
	useEffect(() => {
		let count = 0;
		data.map(i => {
			if (!validPassword(i.password)) count++;
		});
		setCompromisedData(count);
	}, [data]);

	const showOnlyCompromised = () => {
		setFiltered(true);
		setData(data.filter(i => !validPassword(i.password)));
	};

	const showAll = () => {
		setFiltered(false);
		setData(JSON.parse(originalData));
	};

	const searchFilter = key => {
		setFiltered(false);
		const tempData = JSON.parse(originalData);
		setData(tempData.filter(i => i.domain.indexOf(key) > -1));
	};

	return (
		<View>
			<View style={styles.search}>
				<Feather name="search" color="#05375a" size={20} />
				<TextInput
					placeholder="Search"
					style={styles.textInput}
					autoCapitalize="none"
					onChangeText={e => searchFilter(e)}
				/>
			</View>
			<View style={styles.info}>
				<Text style={styles.info_title}>
					We detected {compromisedData} passwords compromised!
				</Text>
				<TouchableOpacity
					onPress={() => (!isFiltered ? showOnlyCompromised() : showAll())}>
					<Text style={styles.info_link}>
						{!isFiltered ? "See only compromised password" : "See All password"}
					</Text>
				</TouchableOpacity>
			</View>
		</View>
	);
}

const styles = {
	info: {
		height: 150,
		backgroundColor: "#8389BC",
		margin: 30,
		marginTop: 5,
		borderRadius: 20
	},
	info_title: {
		color: "#fff",
		fontSize: 25,
		fontWeight: "500",
		margin: 10
	},
	info_link: {
		margin: 10,
		marginTop: 30,
		fontSize: 18,
		color: "#F0C799"
	},
	search: {
		flexDirection: "row",
		margin: 30,
		paddingLeft: 10,
		color: "#05375a",
		backgroundColor: "#A0D1F9",
		opacity: 0.4,
		padding: 15,
		paddingStart: 20,
		borderRadius: 20
	},
	textInput: {
		paddingStart: 10
	}
};

export default PasswordInfo;
