import React from "react";
import Main from "./screens/Main";
import SplashScreen from "./screens/SplashScreen";
import Amplify, { Auth } from "aws-amplify";
Amplify.configure({
	Auth: {
		mandatorySignId: true,
		userPoolId: "ap-southeast-1_0mytnpFdg",
		userPoolWebClientId: "66l90vafmod7domca0ma0mjr5q"
	}
});

export default function App() {
	return <Main />;
}
